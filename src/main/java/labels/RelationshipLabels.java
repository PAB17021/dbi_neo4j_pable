package labels;

public class RelationshipLabels {

    public final static String BOOK_READ = "BOOK_READ";
    public final static String FAMILY_OF = "FAMILY_OF";
    public final static String FRIENDS_OF = "FRIENDS_OF";
    public final static String WRITER_OF = "WRITER_OF";
}
