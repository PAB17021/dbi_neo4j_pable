package neoDao;

import labels.RelationshipLabels;
import model.Person;
import org.neo4j.ogm.session.Session;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PersonsNeo4JDao extends BaseNeo4JDao<Person> {

    public PersonsNeo4JDao(Session neo4jSession) {
        super(neo4jSession);
    }

    @Override
    Class<Person> getMyClass() {
        return Person.class;
    }

    public List<Person> getFamily(Person person) {
        String query = "MATCH (p1) <-[:" + RelationshipLabels.FAMILY_OF + "]-> (p2) " +
                "WHERE (p1.last_name = '" + person.getLastName() + "' AND p2.last_name='" + person.getLastName() + "') " +
                "RETURN p1,p2";
        Map<String, String> map = new HashMap<>();

        Iterable<Person> personIterable = queryEntity(query, map);
        List<Person> out = new ArrayList<>();
        personIterable.forEach(person1 -> out.add(person1));
        return out;
    }

    public List<Person> getFriends(Person person) {
        String query = "MATCH (p1) <-[:" + RelationshipLabels.FRIENDS_OF + "]-> (p2) " +
                "WHERE (p1.last_name='" + person.getLastName() + "')" +
                "RETURN p1,p2";
        Map<String, String> map = new HashMap<>();

        Iterable<Person> personIterable = queryEntity(query, map);
        List<Person> out = new ArrayList<>();
        personIterable.forEach(person1 -> out.add(person1));
        return out;
    }


}
