package neoDao;

import labels.RelationshipLabels;
import model.Book;
import model.Person;
import org.neo4j.ogm.session.Session;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BookNeo4JDao extends BaseNeo4JDao<Book> {

    public BookNeo4JDao(Session neo4jSession) {
        super(neo4jSession);
    }

    @Override
    Class<Book> getMyClass() {
        return Book.class;
    }

    public List<Book> getReadBooks(Person person) {
        String query = "MATCH (b1) <-[:" + RelationshipLabels.BOOK_READ + "]- (p1) " +
                "WHERE (p1.last_name='" + person.getLastName() + "')" +
                "RETURN b1";
        Map<String, String> map = new HashMap<>();
        Iterable<Book> bookIterable = queryEntity(query, map);
        List<Book> res = new ArrayList<>();
        bookIterable.forEach(book -> res.add(book));
        return res;
    }
}
