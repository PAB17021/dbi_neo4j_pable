package neoDao;


import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.transaction.Transaction;

import java.util.Collection;
import java.util.Map;

public abstract class BaseNeo4JDao<T> {

    protected final Session neo4jSession;
    protected Transaction transaction;

    public BaseNeo4JDao(Session neo4jSession) {
        this.neo4jSession = neo4jSession;
        this.transaction = neo4jSession.beginTransaction();
    }

    public Transaction getCurrentTransaction() {
        return neo4jSession.getTransaction();
    }

    public void saveEntity(T entity) {
        neo4jSession.save(entity);
    }

    public void deleteEntity(T entity) {
        neo4jSession.delete(entity);
    }

    public Iterable<T> queryEntity(String query, Map<String, String> map) {
        return neo4jSession.query(getMyClass(), query, map);
    }

    abstract Class<T> getMyClass();

    public Collection<T> getAll() {
        return neo4jSession.loadAll(getMyClass());
    }

    public void commit() {
        transaction.commit();
    }
}