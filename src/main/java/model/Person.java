package model;

import labels.NodeLabels;
import labels.RelationshipLabels;
import org.neo4j.ogm.annotation.Labels;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Property;
import org.neo4j.ogm.annotation.Relationship;

import java.time.LocalDateTime;
import java.util.*;

@NodeEntity
public class Person extends BaseNode {

    @Property(name = "last_name")
    private String lastName;
    @Property(name = "first_name")
    private String firstName;
    @Property(name = "dateOfBirth")
    private String dateOfBirth;
    @Labels
    private List<String> labels = Arrays.asList(NodeLabels.PERSON);
    @Relationship(type = RelationshipLabels.BOOK_READ)
    private Set<Book> readBooks = new HashSet<>();
    @Relationship(type = RelationshipLabels.FAMILY_OF, direction = Relationship.UNDIRECTED)
    private Set<Person> family = new HashSet();
    @Relationship(type = RelationshipLabels.FRIENDS_OF, direction = Relationship.UNDIRECTED)
    private Set<Person> friends = new HashSet<>();

    public Person() {
    }

    protected Person(String firstName, String lastName, LocalDateTime dateOfBirth, Set<Person> friends, Set<Person> family, Set<Book> readBooks) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth.toString();
        this.readBooks = readBooks;
    }

    public static PersonBuilder builder() {
        return new PersonBuilder();
    }

    public void addFamily(Person family) {
        this.family.add(family);
    }

    public void readBook(Book book) {
        this.readBooks.add(book);
    }

    public void addFriend(Person friend) {
        this.friends.add(friend);
    }

    public String getLastName() { return lastName; }

    public String getFirstName() {
        return firstName;
    }

    public LocalDateTime getDateOfBirth() {
        return LocalDateTime.parse(dateOfBirth);
    }

    public List<String> getLabels() {
        return labels;
    }

    public Set<Person> getFriends() {
        return friends;
    }

    public Set<Person> getFamily() {
        return family;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (super.equals(o)) return true;
        Person person = (Person) o;
        return getDateOfBirth() == person.getDateOfBirth() &&
                Objects.equals(getLastName(), person.getLastName()) &&
                Objects.equals(getFirstName(), person.getFirstName()) &&
                Objects.equals(getLabels(), person.getLabels()) &&
                Objects.equals(getFriends(), person.getFriends()) &&
                Objects.equals(getFamily(), person.getFamily()) &&
                Objects.equals(getReadBooks(), person.getReadBooks());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getLastName(), getFirstName(), getDateOfBirth(), getLabels(), getFriends(), getFamily(), getReadBooks());
    }

    public Set<Book> getReadBooks() {
        return readBooks;
    }

    public static class PersonBuilder {
        private String lastName;
        private String firstName;
        private LocalDateTime dateOfBirth;
        private Set<Person> friends = new HashSet<>();
        private Set<Person> family = new HashSet<>();
        private Set<Book> readBooks = new HashSet<>();

        public PersonBuilder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public PersonBuilder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public PersonBuilder dateOfBirth(LocalDateTime dateOfBirth) {
            this.dateOfBirth = dateOfBirth;
            return this;
        }

        public Person build() {
            return new Person(lastName, firstName, dateOfBirth, friends, family, readBooks);
        }
    }
}
