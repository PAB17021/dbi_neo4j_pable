package model;

import org.neo4j.ogm.annotation.*;

import java.util.ArrayList;
import java.util.List;

@NodeEntity
public abstract class BaseNode {

    @Version
    protected Long version;
    @Id
    @GeneratedValue
    private Long id;

    @Labels
    private List<String> labels = new ArrayList<>();

    public BaseNode() {
    }

    public Long getId() {
        return id;
    }

    public Long getVersion() {
        return version;
    }
}
