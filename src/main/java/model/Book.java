package model;

import labels.NodeLabels;
import labels.RelationshipLabels;
import org.neo4j.ogm.annotation.Labels;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Property;
import org.neo4j.ogm.annotation.Relationship;
import org.neo4j.ogm.annotation.typeconversion.DateString;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@NodeEntity
public class Book extends BaseNode {

    @Labels
    private List<String> labels = Arrays.asList(NodeLabels.BOOK);
    @Relationship(type = RelationshipLabels.WRITER_OF)
    private Person writer;
    @Property(name = "name")
    private String name;
    @DateString
    private LocalDateTime releaseDate;

    public Book(Person writer, String name, LocalDateTime releaseDate) {
        this.writer = writer;
        this.name = name;
        this.releaseDate = releaseDate;
    }

    public static PersonBuilder builder() {
        return new PersonBuilder();
    }

    public List<String> getLabels() {
        return labels;
    }

    public Person getWriter() {
        return writer;
    }

    public String getName() {
        return name;
    }

    public LocalDateTime getReleaseDate() {
        return releaseDate;
    }

    public static class PersonBuilder {
        private Person writer;
        private String name;
        private LocalDateTime releaseDate;

        public PersonBuilder writer(Person writer) {
            this.writer = writer;
            return this;
        }

        public PersonBuilder name(String name) {
            this.name = name;
            return this;
        }

        public PersonBuilder releaseDate(LocalDateTime releaseDate) {
            this.releaseDate = releaseDate;
            return this;
        }

        public Book build() {
            return new Book(writer, name, releaseDate);
        }
    }
}
